FROM public.ecr.aws/lambda/provided:latest

RUN yum install -y python3 jq && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    pip3 install awscli

COPY bootstrap /var/runtime/bootstrap
COPY lambda.sh /var/runtime/lambda.sh
RUN chmod 755 /var/runtime/bootstrap

CMD [ "lambda.handler" ]
