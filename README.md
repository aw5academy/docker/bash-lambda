# bash-lambda
This sample code shows 2 ways to create Bash functions in [AWS Lambda](https://aws.amazon.com/lambda/).

In the first method, we package our entire runtime (including our function code) in a [Docker](https://www.docker.com/) image rather than the traditional zip format. This allows you to install extra tools and scripts not normally available to your functions.

The second method creates our custom Bash runtime as a [lambda layer](https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html). In this way, we decouple the function code from the runtime and can reuse the layer across multiple functions.

# Docker

---

## Prerequisites
To test the lambda locally you need to install the [aws-lambda-runtime-interface-emulator](https://github.com/aws/aws-lambda-runtime-interface-emulator/).

On Linux, you can install it with:
```
mkdir -p ~/.aws-lambda-rie && curl -Lo ~/.aws-lambda-rie/aws-lambda-rie https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie && chmod +x ~/.aws-lambda-rie/aws-lambda-rie
```

## Build
Build the docker image with:
```
docker build -t bash-lambda .
```

## Test
Run your container:
```
docker run -p 9000:8080 bash-lambda:latest
```

Now, invoke your function code locally to test:
```
curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d '{"foo":"bar"}'
```

## Publish Docker Image
Follow these steps to publish the image to AWS and create a new function from it.

First, setup some environment variables, replacing the values with your own:
```
export AWS_ACCOUNT_ID=123456789012
export AWS_REGION=us-east-1
export AWS_REGISTRY_HOST="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"
```

Now you can push the image to ECR:
```
aws ecr create-repository --repository-name bash-lambda --region $AWS_REGION
$(aws ecr get-login --no-include-email --registry-ids $AWS_ACCOUNT_ID --region $AWS_REGION)
docker tag bash-lambda:latest $AWS_REGISTRY_HOST/bash-lambda:latest
docker push $AWS_REGISTRY_HOST/bash-lambda:latest
```

## Create Lambda Role
Create the IAM Role we will use for the function with:
```
aws iam create-role --role-name bash-lambda-test --assume-role-policy-document '{"Version": "2012-10-17","Statement": [{ "Effect": "Allow", "Principal": {"Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
aws iam attach-role-policy --role-name bash-lambda-test --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
aws iam attach-role-policy --role-name bash-lambda-test --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
```

## Create Docker Based Lambda Function
Create a new lambda function with:
```
aws lambda create-function \
  --function-name bash-lambda-test \
  --package-type Image \
  --role arn:aws:iam::$AWS_ACCOUNT_ID:role/bash-lambda-test \
  --code ImageUri=$AWS_REGISTRY_HOST/bash-lambda:latest \
  --timeout 30 \
  --memory-size 512 \
  --region $AWS_REGION
```

## Test Docker Based Lambda Function
Test the lambda with:
```
aws lambda invoke --function-name bash-lambda-test --region $AWS_REGION --payload '{ "foo": "bar" }' /tmp/response --log-type Tail --query 'LogResult' --output text |  base64 -d
```

# Lambda Layer

---

## Dependencies
Our function will require `jq` and the [AWS CLI](https://aws.amazon.com/cli/). Run the following to generate these dependencies:
```
mkdir bin lib awscli
docker run -v "$PWD":/var/task "public.ecr.aws/sam/build-provided:latest" /bin/sh -c "cp /usr/bin/jq bin/; cp /usr/lib64/{libjq.*,libonig.so.2} lib/; pip install awscli -t awscli; chmod -R 777 awscli; exit"
mv awscli/* bin/
mv bin/bin/aws bin/aws
rm -rf bin/bin/
sed -i "s,/usr/local/opt/sam-cli/bin/python3,/usr/bin/python3,g" bin/aws
```

## Publish Lambda Layer
Publish the layer with:
```
zip -r runtime.zip bootstrap bin lib
aws lambda publish-layer-version --layer-name bash-runtime --zip-file fileb://runtime.zip --region $AWS_REGION
```

## Create Layer Based Lambda Function
Create a function with:
```
zip function.zip lambda.sh
aws lambda create-function \
  --function-name bash-lambda-layer-test \
  --zip-file fileb://function.zip \
  --handler lambda.handler \
  --runtime provided \
  --role arn:aws:iam::$AWS_ACCOUNT_ID:role/bash-lambda-test \
  --layers arn:aws:lambda:$AWS_REGION:$AWS_ACCOUNT_ID:layer:bash-runtime:1 \
  --timeout 30 \
  --memory-size 512 \
  --region $AWS_REGION
```

## Test Layer Based Lambda Function
Test the lambda with:
```
aws lambda invoke --function-name bash-lambda-layer-test --region $AWS_REGION --payload '{ "foo": "bar" }' /tmp/response --log-type Tail --query 'LogResult' --output text |  base64 -d
```

# Additional Resources:
* [Create Layers With Docker](https://aws.amazon.com/premiumsupport/knowledge-center/lambda-layer-simulated-docker/)
* [Container Image Support](https://aws.amazon.com/blogs/aws/new-for-aws-lambda-container-image-support/)
* [Tutorial - Building a Custom Runtime](https://docs.aws.amazon.com/lambda/latest/dg/runtimes-walkthrough.html)
* [Bash Lambda Layer](https://github.com/gkrizek/bash-lambda-layer)
