handler() {
  EVENT_DATA=$1

  # Get the foo value from event data
  FOO="$(echo "$EVENT_DATA" |jq -r .foo)"

  # Log foo to stderr (prints to CloudWatch Logs)
  echo "Detected foo value is: $FOO" 1>&2;

  # Print S3 buckets
  echo "S3 Buckets List:" 1>&2;
  aws s3 ls 1>&2;

  # Build the JSON response
  RESPONSE="{\"value\":\"$FOO\"}"

  # Return the response
  echo $RESPONSE
}
